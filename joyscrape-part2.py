#!/usr/bin/env python
# encoding: utf8

from __future__ import print_function
import re, urllib, threading, sys, sqlite3, threading, requests, multiprocessing, logging, itertools, requests

try:
    # Python 3
    import urllib.parse as urlparse
    # We're Python 3!
    urllib.urlencode = urllib.parse.urlencode
    import queue as Queue
except:
    # Python 2
    import urlparse
    import Queue
    
import lxml.html as html

SEARCH_ROOT_ENDPOINT = "http://joysound.com/"
SEARCH_ENDPOINT = "http://joysound.com/ex/search/"
SEARCH_HP = "index.htm"

MATCHES_PER_PAGE = 20

HEADERS = {
  "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.43 Safari/537.31",
  "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
  "Accept-Charset": "Shift_JIS,utf-8;q=0.7,*;q=0.3",
  "Accept-Encoding": "gzip,deflate,sdch",
  "Accept-Language": "en-US,en;q=0.8,ja;q=0.6",
  "Cache-Control": "max-age=0",
  "Host": "joysound.com"
}

queue = Queue.Queue()

def write_entry():
    db = sqlite3.connect(sys.argv[1] + ".db")
    c = db.cursor()
    
    # Always drop!
    try:
        c.execute("drop table joysound");
    except sqlite3.OperationalError:
        # No such table
        pass
    c.execute("create table joysound (title text, artist text, lyric text, show text, song_id int, primary key (song_id))")
    
    try:
        i = 0
        while True:
            song_info = queue.get()
            # print(queue.qsize())
            # print(song_info[3])
            c.execute("insert into joysound values (?,?,?,?,?)", song_info)
            db.commit()
            i += 1
            print("Wrote " + str(i))
            queue.task_done()
    except e:
        print(e)
        raise

t = threading.Thread(target = write_entry)
t.daemon = True
t.start()

# Get urls from disk.

info_pages = [s.strip() for s in list(open(sys.argv[1]))]
length = str(len(info_pages))

print("Running info pages")
for i, p in enumerate(info_pages):
    print(str(i + 1) + "/" + length +" " + p)
    r = requests.get(p)
    while r.status_code == 503:
        print("Joysound busy, retrying")
        r = requests.get(p)
    page = html.fromstring(r.text)
    
    info = page.xpath("//img[@alt='HyperJoyV2']/../../..")
    if len(info) == 0: # Don't have this song!
        print("No info.")
        continue
        
    info = info[0]
    title = info.xpath("p[@class='musicName']/a/text()")[0]

    artist = page.xpath("//td[@class='artist']/p/a/text()")[0]
    lyric = page.xpath(u"//th[contains(.,'歌い出し')]/../td")[0]
    if lyric.text is not None:
        lyric = lyric.text
    else:
        lyric = ""
    show = page.xpath(u"//th[contains(.,'情報欄')]/following-sibling::td")[0]
    if show.find("a") is not None:
        show = show.find("a").text
    else:
        show = ""

    song_id = info.xpath("p[@class='musicDetail']/text()")[0]
    song_id = re.search("\d+", song_id).group(0)
    
    queue.put((title, artist, lyric, show, song_id))
