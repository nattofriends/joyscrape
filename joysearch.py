#!/usr/bin/env python
# encoding: utf8

import re, sys, sqlite3

db = sqlite3.connect("joysongs_merged_with_readings.db")
db.row_factory = sqlite3.Row
cur = db.cursor()

fields = ["artist", "title", "show"]
REGEX = "({fields}):(.*?)(?:;|$)".format(fields = "|".join(fields))

def search(query):
    res = re.findall(REGEX, query)
    constraints = []
    for co in res:
        if co[0] not in fields:
            # NO FUQU
            continue
        constraint = u"{} like '%%{}%%'".format(*co)
        reading_constraint = u"song_id in (select song_id from {}_readings where reading like '%%{}%%')".format(*co)
        both = "(" + constraint + " or " + reading_constraint + ")"
        constraints.append(both)

    constraints = u" and ".join(constraints)

    sql = u"select * from joysound where {constraints}".format(constraints = constraints).encode("utf8")
    # print sql
    
    ret = list(cur.execute(sql))
    return ret

query = u"show:ひぐらし; artist:島みや"
# print search(query)
for s in search(query):
    print s["title"].encode("utf8"), s["show"].encode("utf8")