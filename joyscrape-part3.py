#!/usr/bin/env python
# encoding: utf8

import regex, MeCab, os, jcconv, itertools, sqlite3, sys

CHUNK = 1000

def ipadic_handler(tagged):
    surface, feature = tagged.split("\t")
    try: 
        return jcconv.kata2hira(feature.split(",")[7])
    except IndexError:
        return jcconv.kata2hira(surface)
        
def juman_handler(tagged):
    surface, feature = tagged.split("\t")
    t = feature.split(",")[5]
    if t == "*":
        return jcconv.kata2hira(surface)
    return jcconv.kata2hira(t)
    
def unidic_handler(tagged):
    """UniDic seems to derp often too. Forgetting it..."""
    tagged = tagged.split("\t")
    return jcconv.kata2hira(tagged[2])
    
grabbers = {
    # Too bad! juman appears to be a piece of shit! At least we have the support for multiple dictionaries.
    # I want to enable this just in case, but its shittiness is surprising me.
    # "juman-utf8": juman_handler,
    # "ipadic-utf8": ipadic_handler,
    # naist-jdic is an improved version of ipadic.
    "naist-jdic": ipadic_handler,
    # "unidic": unidic_handler
}

models = {dict: MeCab.Model("-d /var/lib/mecab/dic/{dict}".format(dict = dict)) for dict in grabbers.keys()}
# It's not happy if we don't keep a explicit reference to  it for some reason...
taggers = {dict: model.createTagger() for dict, model in models.items()}

def make_reading(string):
    ret = []
    for k in grabbers.keys():
        t = taggers[k]
        grabber = grabbers[k]
        
        res = t.parse(string.encode("utf8")).split("\n")[:-2]

        # Raw feature output
        # print "\n".join(res)
        features = [grabber(t) for t in res]
        ret.append("".join(features))
    return list(set(ret))

def sub_readings(string):
    ret = []
    chunks = regex.findall(u"[\p{Hiragana}\p{Katakana}\p{Han}]+", string, flags = regex.UNICODE)
    replace_target = regex.sub(u"[\p{Hiragana}\p{Katakana}\p{Han}]+", "{}", string, flags = regex.UNICODE).encode("utf8")
    readings = map(make_reading, chunks)
    reading_applier = itertools.product(*readings)
    for reading in reading_applier:
        # print " ".join(reading)
        ret.append(replace_target.format(*reading))
    return ret
    
def try_drop(cur, tbl):
    try:
        cur.execute("drop table {tbl}".format(tbl = tbl))
    except sqlite3.OperationalError:
        pass

def creat(cur, tbl):
    cur.execute("create table {tbl} (reading text, song_id int, primary key (song_id))".format(tbl = tbl))

db = sqlite3.connect("joysongs_merged_with_readings.db")
db.row_factory = sqlite3.Row
cur = db.cursor()

tables = ["artist_readings", "title_readings", "show_readings"]
map(lambda t: try_drop(cur, t), tables)
map(lambda t: creat(cur, t), tables)

cur.execute("select count(*) from joysound")
num_rows =  cur.fetchone()[0]

for i in range((num_rows / CHUNK) + 1):

    title_reading_tuples = []
    artist_reading_tuples = []
    show_reading_tuples = []

    for j, row in enumerate(cur.execute("select * from joysound limit {from_row}, {chunk}".format(from_row = i * CHUNK, chunk = CHUNK))):
        if j % 10 == 0:
            sys.stdout.write(str(i * CHUNK + j) + "/" + str(num_rows) + "\r")
            sys.stdout.flush()
            
        title_readings = sub_readings(row["title"])
        for r in title_readings:
            title_reading_tuples.append((r.decode("utf8"), row["song_id"]))
        
        artist_readings = sub_readings(row["artist"])
        for r in artist_readings:
            artist_reading_tuples.append((r.decode("utf8"), row["song_id"]))

        show_readings = sub_readings(row["show"])
        for r in show_readings:
            show_reading_tuples.append((r.decode("utf8"), row["song_id"]))
            
    cur.executemany("insert into title_readings values (?,?)", title_reading_tuples)
    cur.executemany("insert into artist_readings values (?,?)", artist_reading_tuples)
    cur.executemany("insert into show_readings values (?,?)", show_reading_tuples)

    del title_reading_tuples
    del artist_reading_tuples
    del show_reading_tuples
    
    db.commit()
    
db.close()