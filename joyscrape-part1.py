#!/usr/bin/env python
# encoding: utf8

# Now we know: grequests.imap is unordered. ~_~

from __future__ import print_function
import re, urllib, threading, sys, sqlite3, threading, requests, multiprocessing, logging, itertools

try:
    # Python 3
    import urllib.parse as urlparse
    # We're Python 3!
    urllib.urlencode = urllib.parse.urlencode
    import queue as Queue
except:
    # Python 2
    import urlparse
    import Queue

from requests import get as force_get    
# try:
    # import grequests as requests
    # print("info: using grequests")
    # imap = lambda func, iter: requests.imap((func(_) for _ in iter))
# except:
import requests
print("info: using requests")
imap = lambda func, iter: itertools.imap(func, iter)
    
import lxml.html as html

SEARCH_ROOT_ENDPOINT = "http://joysound.com/"
SEARCH_ENDPOINT = "http://joysound.com/ex/search/"
SEARCH_HP = "index.htm"

MATCHES_PER_PAGE = 20

# Get the head pages.

r = force_get(SEARCH_ENDPOINT + SEARCH_HP)
while r.status_code != 200:
    r = force_get(SEARCH_ENDPOINT + SEARCH_HP)
s = html.fromstring(r.text)

head_pages = s.xpath("//img[@src='/ex/search/img/SA05_ti3.gif']/../../div[@class='searchBoxArea02']//a")
head_pages = [SEARCH_ENDPOINT + e.attrib["href"] for e in head_pages]

print(str(len(head_pages)) + " head pages")

search_pages = []

indicators = list("-/|\\")

print("initializing search pages")
for i, (p, r) in enumerate(itertools.izip(head_pages, imap(requests.get, head_pages))):
    # print(str(i + 1) + "/" + str(len(head_pages)) + " " + p)
    sys.stdout.write("\r" + indicators[i % len(indicators)] + " " + str(i + 1) + "/" + str(len(head_pages)))
    while r.status_code == 503:
        # print("Joysound busy, retrying")
        r = force_get(p)
    page = html.fromstring(r.text)
    
    num_matches = page.xpath("//span[@class='hit']")
    if len(num_matches) == 0: # Nothing starts with this letter!
        num_matches = 0
    else:
        num_matches = int(num_matches[0].text)
        
    num_pages = int(num_matches / MATCHES_PER_PAGE) + 1
    # print(num_pages)    
    # search_pages = itertools.chain(search_pages, [p + "&offset=" + str(i * MATCHES_PER_PAGE) for i in range(num_pages)])
    search_pages.extend([p + "&offset=" + str(i * MATCHES_PER_PAGE) for i in range(num_pages)])

info_pages = []
urls_put = open("joysongs.txt", 'w')

print("\nrunning search pages")
for i, (p, r) in enumerate(itertools.izip(search_pages, imap(requests.get, search_pages))):
    # print(str(i + 1) + "/infty " + p)
    sys.stdout.write("\r" + indicators[i % len(indicators)] + " " + str(i + 1) + "/" + str(len(search_pages)))
    while r.status_code == 503:
        # print("Joysound busy, retrying")
        r = force_get(p)
    page = html.fromstring(r.text)
    
    # songs = page.xpath("//td[@class='title']/a")
    songs = page.xpath("//li[@class='kara']/ancestor::tr[1]/preceding-sibling::tr[1]/td[@class='title']/a")
    songs = (SEARCH_ROOT_ENDPOINT + e.attrib["href"] for e in songs)
    urls_put.write("\n".join(songs) + "\n")
    
print("Done.")
