joyscrape
========

What is this?
-------------
joyscrape parses the [Joysound database](http://www.joysound.com) for song information for a particular model (selectable) of Joysound karaoke machine.

Requirements
------------
pip-installable requirements are listed in Requirements.txt.
MeCab and the NAIST dictionary are also required. On Debian, this is `python-mecab` and `mecab-maist-jdic`.

Usage
-----
1. Run `./joyscrape-part1.py` to get a list of song URLs. This creates `joysongs.txt`.
2. Run `./joyscrape-part1.5.sh NUM` to clean and split `joysongs.txt`. Because Joysound is slow, you should split to the number of machines you have, and run the resulting split files on different machines. This creates `joysplit_{a,b,c...}`.
3. On your multiple machines, run `./joyscrape-part2.py SPLIT_FILE` to download the individual song information. This will create `SPLIT_FILE.db`.
4. Copy the `SPLIT_FILE.db`s back to your first machine, and combine them in `sqlite3`, like so: 

```
$ sqlite3 joysongs_merged.db
sqlite3> create table joysound (title text, artist text, lyric text, show text, song_id int, primary key (song_id));
sqlite3> attach 'joysplit_a.db' as a;
sqlite3> begin;
sqlite3> insert into joysound select * from a.joysound;
sqlite3> commit;
```
5. Run `./joyscrape-part3.py` to generate the auxiliary readings tables. This creates `joysongs_merged_with_readings.db`.
